FROM harbor.sbereducation.site/hub/library/python:3.10.2

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . .

CMD [ "python3", "main.py"]

EXPOSE 8000/tcp