import requests
from collections import OrderedDict

r = requests.get("https://www.eve-scout.com/api/wormholes")

json_data = r.json()

sorted_data = sorted(json_data, key=lambda k: k.get('id'))

for element in sorted_data:

    print(element["id"],element["destinationSolarSystem"]["name"])
